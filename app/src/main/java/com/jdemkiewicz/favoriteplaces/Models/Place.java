package com.jdemkiewicz.favoriteplaces.Models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by j.demkiewicz on 14.08.2017.
 */

public class Place extends RealmObject {
    @PrimaryKey
    private int id;

    private String name;
    private String description;
    private double latitude;
    private double longitude;
    private String address;

    public Place() {
    }

    public Place(int id, String name, String description, String address, double latitude, double longitude) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public String getAddress() {
        return address;
    }
}
