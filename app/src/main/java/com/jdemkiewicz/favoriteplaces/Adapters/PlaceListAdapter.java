package com.jdemkiewicz.favoriteplaces.Adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jdemkiewicz.favoriteplaces.Models.Place;
import com.jdemkiewicz.favoriteplaces.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.internal.Collection;

/**
 * Created by j.demkiewicz on 14.08.2017.
 */

public class PlaceListAdapter extends RecyclerView.Adapter<PlaceListAdapter.PlaceViewHolder> {
    public static final String TAG = PlaceListAdapter.class.getSimpleName();
    private final RealmResults<Place> places;

    public PlaceListAdapter(RealmResults<Place> places) {
        this.places = places;
    }

    @Override
    public PlaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_card_layout, parent, false);
        return new PlaceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PlaceViewHolder holder, final int position) {
        final Place place = places.get(position);
        holder.placeName.setText(place.getName());
        holder.location.setText(place.getAddress());
        holder.description.setText(place.getDescription());
        // TODO: 14.08.2017 change position
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, places.size() + "");
                holder.delete.setVisibility(View.GONE);
                deletePlace(place, holder.getAdapterPosition());
            }
        });
    }

    private void deletePlace(final Place place, final int position) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                place.deleteFromRealm();
                notifyItemRemoved(position);
                Log.d(TAG, places.size() + "");

            }
        });
    }

    @Override
    public int getItemCount() {
        return places.size();
    }

    static class PlaceViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.card_view_place_name)
        TextView placeName;

        @BindView(R.id.cardView)
        CardView cardView;

        @BindView(R.id.card_view_place_description)
        TextView description;

        @BindView(R.id.card_view_place_location)
        TextView location;

        @BindView(R.id.card_view_place_delete)
        ImageView delete;

        public PlaceViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
