package com.jdemkiewicz.favoriteplaces;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.jdemkiewicz.favoriteplaces.Adapters.PlaceListAdapter;
import com.jdemkiewicz.favoriteplaces.Models.Place;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by j.demkiewicz on 14.08.2017.
 */

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.main_recycler_container)
    RecyclerView recyclerView;

    private Realm realm;
    private RealmResults<Place> places;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        realm = Realm.getDefaultInstance();
        places = realm.where(Place.class).findAllSorted("id", Sort.DESCENDING);
        setupRecyclerView();
    }

    private void setupRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new PlaceListAdapter(places));
    }

    @OnClick(R.id.main_fab)
    public void onAddPlaceButtonClicked() {
        Intent intent = new Intent(this, AddPlaceActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshList();
    }

    private void refreshList() {
        places = realm.where(Place.class).findAllSorted("id", Sort.DESCENDING);
        recyclerView.setAdapter(new PlaceListAdapter(places));
    }
}
