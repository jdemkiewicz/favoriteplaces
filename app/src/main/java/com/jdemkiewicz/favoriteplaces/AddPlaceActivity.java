package com.jdemkiewicz.favoriteplaces;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.jdemkiewicz.favoriteplaces.Models.Place;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

/**
 * Created by j.demkiewicz on 14.08.2017.
 */

public class AddPlaceActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        TextWatcher {
    public static final String TAG = AddPlaceActivity.class.getSimpleName();
    public static final int PERMISSION_ACCESS_FINE_LOCATION_REQUEST_CODE = 1;
    private Realm realm;
    private GoogleApiClient googleApiClient;
    @BindView(R.id.add_place_activity_description)
    EditText description;
    @BindView(R.id.add_place_activity_place_name)
    EditText placeName;
    @BindView(R.id.add_place_activity_latitude)
    TextView latitudeLocation;
    @BindView(R.id.add_place_activity_longitude)
    TextView longitudeLocation;
    @BindView(R.id.add_place_activity_address)
    EditText addressEditText;

    private double latitude;
    private double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_place);
        ButterKnife.bind(this);
        realm = Realm.getDefaultInstance();
        checkPermission();
        googleApiClient = new GoogleApiClient.Builder(this, this, this).addApi(LocationServices.API).build();
        placeName.addTextChangedListener(this);
    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_ACCESS_FINE_LOCATION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ACCESS_FINE_LOCATION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    displayToast("Turn on location permission");
                }
                break;
        }
    }

    @OnClick(R.id.add_place_activity_get_location)
    public void onGetLocationClicked() {
        googleApiClient.connect();
    }

    @OnClick(R.id.add_place_activity_save)
    public void onSaveButtonClicked() {
        Place place = getNewPlace();
        if (!place.getName().trim().isEmpty()) {
            savePlaceToRealm(place);
            googleApiClient.disconnect();
            finish();
        } else {
            placeName.setError("This field can not be blank");
        }
    }

    private Place getNewPlace() {
        String name = placeName.getText().toString();
        String desc = description.getText().toString();
        String address = addressEditText.getText().toString();
        return new Place(generateId(), name, desc, address, latitude, longitude);
    }

    private int generateId() {
        Number maxNumber = realm.where(Place.class).max("id");
        return maxNumber != null ? maxNumber.intValue() + 1 : Integer.MIN_VALUE;
    }

    private void savePlaceToRealm(final Place place) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealm(place);
            }
        });
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        createAlertDialog(alertDialogBuilder);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void createAlertDialog(AlertDialog.Builder alertDialogBuilder) {
        alertDialogBuilder.setMessage("Are you sure you want to quit without saving?")
                .setCancelable(true)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        googleApiClient.disconnect();
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if (lastLocation != null) {
                latitude = lastLocation.getLatitude();
                longitude = lastLocation.getLongitude();
                addressEditText.setText(getAddressFromCoordinates(lastLocation.getLatitude(), lastLocation.getLongitude()));
            }
        }
    }

    private String getAddressFromCoordinates(double latitude, double longitude) {
        Geocoder geocoder;
        List<Address> addresses;
        String address = "";
        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            address = addresses.get(0).getAddressLine(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d(TAG, address);
        return address;
    }

    @Override
    public void onConnectionSuspended(int i) {
        displayToast("Problem with GPS connection");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        displayToast("Problem with GPS connection");
    }

    private void displayToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (placeName.getText().toString().length() <= 0) {
            placeName.setError("Enter FirstName");
        } else {
            placeName.setError(null);
        }
    }
}
