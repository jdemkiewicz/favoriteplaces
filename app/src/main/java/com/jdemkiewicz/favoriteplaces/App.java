package com.jdemkiewicz.favoriteplaces;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by j.demkiewicz on 14.08.2017.
 */

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
    }
}
